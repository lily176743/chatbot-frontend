export default function ServiceLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return <div>This is ServiceLayout page.{children}</div>;
}
